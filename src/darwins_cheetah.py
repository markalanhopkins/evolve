from src.evolution_base import Population, Individual
import time
import numpy as np

"""Evolutionary algorithm framework applied to a simplified biological model."""

class Cheetah(Individual):
    """A cheetah consists of a bit string representing its DNA, and a fitness score.

    Example:
        cheetah.dna='000110011111000111001'
        cheetah.fitness= 0.6

    The concept is that each bit contributes to a particular physical trait that helps the cheetah
    survive in its environment. The first three could modify eyesight, the next six speed, fang length etc.

    In this simplified example it's not important that we map DNA to a particular physical phenotype
    or know how this interacts with the environment. The goal is just to breed the optimal genotype.
    The target DNA will be provided and a random population of cheetahs will evolve toward it.
    """
    def __init__(self, dna='', fitness=0):
        """
        Args:
             dna: Bit string representing DNA e.g '000110011111000111001'
             fitness: Typically set to 0 on creation.
        """
        super().__init__(fitness)
        self.dna = dna


    @staticmethod
    def get_random_dna(dna_length):
        """Return random string of 1's and 0's
        Args:
            dna_length: Number of bits
        Returns:
            String representation of simplified 0/1 DNA eg '0111001100101'
        """
        genes = ''
        for i in range(dna_length):
            genes = genes + str((np.random.randint(0,2)))
        return genes


class Cheetah_Population(Population):
    """Aggregates Individuals into generations and defines core evolutionary methods to evolve the optimal cheetah"""
    def __init__(self, optimal_dna, tournament_size, mutation_rate):
        """
        Args:
            optimal_dna: Target genotype to evolve toward.
            tournament_size: The number of random individuals making up the tournament used to simulate
                             competition and mate selection.
            mutation_rate: The rate at which the child generation's DNA is mutated after breeding.
        """
        super().__init__(tournament_size, mutation_rate)
        self.optimal_dna = optimal_dna
        self.dna_length = len(optimal_dna)


    def add_random_individual(self):
        """Seeds the first generation"""
        random_dna = Cheetah.get_random_dna(self.dna_length)
        new_cheetah = Cheetah(random_dna)
        self.individuals.append(new_cheetah)


    def calc_fitness(self, individual):
        """A cheetah's fitness is the degree to which it concords, bit by bit to the known optimal

        Example:
            optimal:      '111000111'
            individual:   '110000001'
            fitness = 6/9 for each of the concordant 0's and 1's

        This massively simplifies the real world genotype-to-phenotye(body) mapping, and the details of
        how the environment applies the selection pressure. A more complex model would define these in detail
        so as to derive an optimal DNA sequence from the interaction between physical phenotype and environment.
        We are assuming this is already known and are only working at the genotype level.

        Args:
            individual: Cheetah containing DNA sequence, e.g. '000110011111000111001'.

        Returns:
            float ratio of bit-concordance with optimal DNA.
        """
        concord = 0
        # TODO performance isn't an issue in this example but consider refactoring the bits to an array
        for i in range(self.dna_length):
            if individual.dna[i] == self.optimal_dna[i]:
                concord += 1

        individual.fitness = concord / float(self.dna_length)



    def mutate(self, individual):
        """Flip the 0 or 1 bit in the DNA at the specified mutation rate."""
        mutated = ''
        for i in range(self.dna_length):
            if np.random.rand() < self.mutation_rate:
                mutated += str(1-int(individual.dna[i]))
            else:
                mutated += individual.dna[i]

        individual.dna = mutated


    def gene_crossover(self, first_parent, second_parent):
        """Creates new child cheetah. Child DNA is a random combination of the parent genotypes.

        Returns:
            New cheetah bred from parents
        """
        child = Cheetah()
        for i in range(self.dna_length):
            if np.random.rand() > self.crossover_rate:
                child.dna += first_parent.dna[i]
            else:
                child.dna += second_parent.dna[i]

        return child



    def halt_evolution(population, total_generations, total_individuals, optimal_cheetah_dna):
        """Defines success and/or failure.

        Args:
            population: Current generation
            total_generations: Number of existing generations
            total_individuals: Number of existing cheetahs
            optimal_cheetah_dna: Known optimal

        Returns:
            Boolean
        """
        good_enough = 1.0
        """Evolving the last 10% of the optimal genotype typically takes most of the iterations. Set lower as needed."""

        return population.individuals[0].dna == optimal_cheetah_dna or \
               population.individuals[0].fitness > good_enough



def evolve_cheetah(optimal_cheetah_dna):
    """Starting with a randomly seeded generation of cheetahs, evolve toward known optimal DNA."""
    generation_size = 32
    mutation_rate = 0.04
    tournament_size = 5

    # Seed first generation
    parent_generation = Cheetah_Population(optimal_cheetah_dna,  tournament_size, mutation_rate)
    for i in range(generation_size):
        parent_generation.add_random_individual()

    total_generations = 1
    total_individuals = generation_size

    # Main evolution loop
    while True:
        parent_generation.sort_population_by_fitness()

        #Success or failure break
        if Cheetah_Population.halt_evolution(parent_generation, total_generations, total_individuals, optimal_cheetah_dna):
            break

        fittest = parent_generation.individuals[0]
        print('%s : Generation %d   %.1f %% optimal DNA' % (fittest.dna, total_generations, fittest.fitness * 100))

        # Empty child generation
        child_generation = Cheetah_Population(optimal_cheetah_dna, tournament_size, mutation_rate)
        start_index = 0 # Not implementing elitism, loser-invert or alternate breeding strategies
        parent_generation.breed_new_generation(parent_generation, child_generation, start_index)
        child_generation.mutate_pop(start_index)

        total_generations += 1
        total_individuals += generation_size
        parent_generation = child_generation
        # time.sleep(1.5)

    print('\n\nTotal Generations %d \nTotalIndividuals %d' % (total_generations, total_individuals))
    fittest_evolved_cheetah =  parent_generation.individuals[0]

    return fittest_evolved_cheetah



if __name__ == '__main__':
    # Choose a visually recognizable bit pattern to see the evolution play out in real-time
    optimal_cheetah_dna = '1110000000000001110000000000000000000111000000000000000000000000011100000000000000000000111'
    evolved_cheetah_dna = evolve_cheetah(optimal_cheetah_dna)
    print('Evolved optimal cheetah DNA:\n' + evolved_cheetah_dna.dna)

