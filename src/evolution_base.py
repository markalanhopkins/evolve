import numpy as np
from abc import ABC, abstractmethod

""" Evolutionary algorithm framework.

Contains the abstract base classes of Population and Individual. Population aggregates Individuals into
generations and contains the core methods for competition, mate-selection, breeding, gene crossover and mutation. 
Some of these methods are domain-specific and will be defined at the subclass level, others are generic and 
defined here. 
"""


class Population(ABC):
    """Aggregates Individuals into generations and defines core evolutionary methods.

    Each generation consists of a Population of the same size. The typical process is to seed
    the first generation randomly, then evolve subsequent generations based on the darwinian
    concepts of fitness, mate selection, gene crossover, and mutation. Subclasses must define these
    core concepts according to their domain. Non-darwinian concepts like elite-survival, loser-inversion
    and experimental breeding strategies can also be implemented."""

    def __init__(self, tournament_size, mutation_rate, data=None,
                 crossover_rate=0.5, fitter_is_higher=True):
        """
        Args:
            data: Subclasses will typically use this to calculate an individual's fitness.
            tournament_size: A tournament is one common method of mate-selection.
            mutation_rate: The rate at which to randomly mutate the child's DNA after breeding.
            crossover_rate: The ratio at which one parent's genes are favored over the other. Typically equal.
            fitter_is_higher: Determines fitness ordering of individuals.
        """
        self.individuals = []
        self.data = data
        self.tournament_size = tournament_size
        self.mutation_rate = mutation_rate
        self.crossover_rate = crossover_rate
        self.fitter_is_higher = fitter_is_higher


    def get_pop_size(self):
        return len(self.individuals)

    def add(self, individual):
        self.individuals.append(individual)

    @abstractmethod
    def add_random_individual(self):
        """" Subclasses must implement a method that defines the addition of a random individual to the Population.
        This will typically be used to seed the first generation.
        """

    @abstractmethod
    def calc_fitness(self, individual):
        """Subclasses must implement a method that calculates the fitness of an individual. This is entirely
        domain specific. Fitness could be a finance sharpe-ratio, a particular sequence of bits representing DNA,
        or hyper-parameters used in spawning other evolutionary algorithms.
        """

    @abstractmethod
    def mutate(self, individual):
        """Subclasses must implement a method that mutates an individual. Key to this process is mutation rate.
        Too little mutation and a Population will plateau and not evolve to optimal fitness. Too much mutation
        and the evolved survival traits will quickly be lost in the random fluctuations
        """

    @abstractmethod
    def gene_crossover(self, individual_a, individual_b):
        """Subclasses must implement a method that combines the genes of two Individuals to create a child."""

    @staticmethod
    @abstractmethod
    def halt_evolution(*args, **kwargs):
        """Subclasses must implement a method to define success and/or failure.

        The halt condition could be a known DNA target, a fitness threshold, a specified number of generations,
        a minimum fitness-gradient between generations, a reduction in variance amongst individuals etc.
        """

    def sort_population_by_fitness(self):
        """Sorts individuals based on the criteria defined in calc_fitness(). Crucial for mate selection."""
        for individual in self.individuals:
            self.calc_fitness(individual)
        self.individuals = sorted(self.individuals, key=lambda i: i.fitness,
                                  reverse=self.fitter_is_higher)

    def mutate_pop(self, mutation_index):
        """Iterates through the generation, mutating individuals at the specified rate.

        Args:
            mutation_index: The first n slots of a new generation can be filled with special cases. E.g.
                            a copy of the previous generation's fittest individual, an inverted loser,
                            elite-bred children etc. Typically, these special individuals are not mutated
                            and the mutation_index defines the first individual that will be.
        """
        for child_index in range(mutation_index, self.get_pop_size()):
            if np.random.rand() <= self.mutation_rate: # This decides whether an individual is mutated at all.
                child = self.individuals[child_index]
                self.mutate(child) # Subclasses will define how and to what extent an individual is mutated.


    def tournament(self):
        """Primary method of mate selection. Each parent will be the fittest of n randomly chosen individuals."""
        contestants = []
        pop_size = len(self.individuals)
        assert self.tournament_size <= pop_size
        while len(contestants) < self.tournament_size:
            random_individual = np.random.choice(self.individuals)
            if random_individual not in contestants:
                contestants.append(random_individual)

        contestants = sorted(contestants, key=lambda i: i.fitness, reverse=self.fitter_is_higher)
        return contestants[0]


    @staticmethod
    def breed_new_generation(parent_gen, child_gen, start_index):
        """For each slot in an empty child generation, use tournament selection to choose two parents from the
        previous generation and run the gene crossover. All generations are the same size.

        Args:
            parent_gen: Population of potential parents
            child_gen: Typically empty
            start_index: The first n slots of a new generation can be filled with special cases e.g.
                         a copy of the previous generation's fittest individual, an inverted loser,
                         elite-bred children etc. This parameter keeps track of where normal breeding starts.
        Returns:
            The newly bred child generation.

        """
        for child_index in range(start_index, parent_gen.get_pop_size()):
            parent_a = parent_gen.tournament()
            parent_b = parent_a
            while parent_b == parent_a:
                parent_b = parent_gen.tournament()

            child = parent_gen.gene_crossover(parent_a, parent_b)
            child_gen.add(child)


class Individual(ABC):
    """Individuals are aggregated into Populations, assigned fitness, selected-for and inter-bred.
    Subclasses will define the domain-specific attributes and behavior.
    """
    def __init__(self, fitness):
        """
        Args:
            fitness: A score to rank Individuals for mate-selection and breeding.
        """
        self.fitness = fitness


def jitter(minimum, current, maximum, amplitude, manual_percent_change=None):
    """Returns a new value some percentage of the way between current and either min or max.

    Args:
        minimum: The lowest possible return value.
        current: Input.
        maximum: The highest possible return value.
        amplitude: Dampen the random variance.
        manual_percent_change: Disables the random variance.

    Returns:
        The new value
    """
    if not manual_percent_change:
        raw_percent_change = np.random.uniform(low=-1,high=1)
    else:
        raw_percent_change = manual_percent_change
    percent_change = raw_percent_change * amplitude

    if percent_change > 0:
        gap = maximum - current
    else:
        gap = current - minimum

    jittered = current + (percent_change * gap)  # self-adjusts for direction based on sign of percent_change
    return jittered

