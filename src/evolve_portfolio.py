import numpy as np
import pandas as pd
import copy
from src.evolution_base import Individual, Population, jitter
from math import isclose
import quandl
from matplotlib import pyplot as plt

"""Evolutionary algorithm framework applied to optimizing a stock market portfolio. 

Example:
    with $1000 to spend, portfolio weights could be: aapl:$300  cisco:$100  ibm:$200  amzn:$400

    Optimal in this context is a maximised sharpe ratio, the standard measure of return vs risk
    over a given period of time. Different weightings of the same stocks will have different sharpe ratios. 
    The goal is to breed the optimal weightings given the historical price data.
    
Core methods defined in the framework are used along with non-darwinian concepts like elite-survival, 
loser-inversion and various experimental breeding strategies.

This example demonstrates the efficiency of an evolved solution vs a brute-force approach. The optimum weights are 
typically evolved in less than 10 generations comprising around 500 individuals. The brute force approach 
does less well even with 30,000+ random weightings.
"""

class Stock_Weights_Individual(Individual):
    """ An array of weights for each of the stock symbols. Sums to 1.0.
        Example:
         symbols = ['AAPL', 'CISCO', 'IBM', 'AMZN']
         weights = [.3, .2, .27 , .23]
    """
    def __init__(self, symbols, weights):
        super().__init__(fitness=0)
        self.symbols = symbols
        self.weights = np.array(weights)

    def __repr__(self):
        s = ''
        for symbol, weight in zip(self.symbols, self.weights):
            s += format('%s\t%.2f \n' % (symbol, weight))
        s += format('expected return \t %.5f \nexpected volatility \t %.5f \nsharpe \t\t %.5f' % (
        self.expected_return, self.expected_volatility, self.fitness))
        return s


class Weights_Population(Population):
    """Aggregates Stock_weights_Individuals into generations and defines core evolutionary methods for this domain."""
    SYMBOLS = 'AAPL CISCO IBM AMZN'.split()
    """Stocks used in this example"""

    OPTIMAL_SHARPE = 1.03
    """Derived from previous brute-force tests and is near maximal for these stocks."""

    def __init__(self, data, tournament_size, mutation_rate, mutation_amplitude):
        """
        Args:
            data: Historical stock data.
            tournament_size: Number of individuals to include in the tournament for mate-selection.
            mutation_rate: The rate at which to randomly mutate the child generation's DNA after breeding.
            mutation_amplitude: Dampening.
        """
        super().__init__(tournament_size, mutation_rate, data)
        self.mutation_amplitude = mutation_amplitude


    def add_random_individual(self):
        """Seeds first generation."""
        weights = np.array(np.random.random(4))
        # Rebalance weights to sum to 1.0
        weights = weights / np.sum(weights)
        individual = Stock_Weights_Individual(self.SYMBOLS, weights)
        self.add(individual)


    @staticmethod
    def rebalance(weights, new_weight, new_weight_index):
        """Replace one weight and rebalance remaining. Maintains proportionality and sum=1.0"""
        weights[new_weight_index] = new_weight
        new_remainder = 1 - new_weight
        remaining_weight_sum = np.sum(weights) - weights[new_weight_index]
        # keep the relative proportions of the remaining weights
        for i in range(len(weights)):
            if i != new_weight_index:
                weights[i] = (weights[i] / remaining_weight_sum) * new_remainder

        assert isclose(1, np.sum(weights), abs_tol=0.00001)


    def mutate(self, individual):
        """Modify a randomly chosen weight by a random amount. Rebalance remaining weights."""
        new_weight_index = np.random.randint(0, len(individual.weights))
        random_percent_change = np.random.uniform(-1, 1)
        # Modify with mutation amplitude
        percent_change = self.mutation_amplitude * random_percent_change
        old_weight = individual.weights[new_weight_index]
        new_weight = jitter(minimum=0, current=old_weight, maximum=1, amplitude=1, manual_percent_change=percent_change)
        self.rebalance(individual.weights, new_weight, new_weight_index)


    @staticmethod
    def gene_crossover(parent_a, parent_b):
        """Creates new child. Each child weight is the halfway point of the two parent weights. Maintains sum=1.0"""
        child_weights = []
        for parent_a_weight, parent_b_weight in zip(parent_a.weights, parent_b.weights):
            child_weights.append((parent_a_weight + parent_b_weight) / 2.0)

        child = Stock_Weights_Individual(Weights_Population.SYMBOLS, child_weights)
        assert isclose(np.sum(child.weights), 1, abs_tol=0.00001)
        return child


    def calc_fitness(self,individual):
        """Uses the specific stock weightings for this individual and the historical price data to calculate
        the expected return vs the expected volatility. In finance this is known as the sharpe ratio.
        """
        annualised_multiplier = 252
        individual.expected_return = np.sum(self.data.mean() * individual.weights) * annualised_multiplier
        individual.expected_volatility = np.sqrt(np.dot(individual.weights.T, np.dot(self.data.cov() * annualised_multiplier, individual.weights)))
        sharpe_ratio = individual.expected_return / individual.expected_volatility
        individual.fitness = sharpe_ratio


    @staticmethod
    def invert(individual):
        """Invert each weight and rebalance to maintain sum=1.0."""
        inverted_raw = np.array([1 - i for i in individual.weights])
        inverted_rebalance = inverted_raw / np.sum(inverted_raw)
        individual.weights = inverted_rebalance


    @staticmethod
    def elite_mate_selection(individuals, num_elite_children):
        """Pairs off the fittest individual with the second fittest, second fittest with third etc.

        Returns: List of parent index-pairings for elite breeding.
        """
        pairings = []
        liaisons = 0
        first = 0
        second = 1
        while (liaisons < num_elite_children) and (second < len(individuals)):
            while individuals[second].fitness == individuals[first].fitness:
                second += 1 # Advance index until the first non-equal fitness is found.
            pairings.append((first, second))
            liaisons += 1
            first = second
            second += 1

        return pairings


    def halt_evolution(parent_generation, total_individuals, max_individuals):
        """Success and failure conditions."""
        return parent_generation.individuals[0].fitness > Weights_Population.OPTIMAL_SHARPE \
               or total_individuals > max_individuals


    def __repr__(self):
        """Show the top fittest n individuals and the bottom n """
        s = 'Top:\n'
        n=5
        for individual in self.individuals[:n]:
            s += format('Sharpe: %.5f:  Weightings: %.3f  %.3f  %.3f  %.3f \n' % (individual.fitness,
                                                             individual.weights[0],
                                                             individual.weights[1],
                                                             individual.weights[2],
                                                             individual.weights[3]))
        s += '....\nBottom:\n'
        for individual in self.individuals[(-1*n):]:
            s += format('Sharpe: %.5f: Weightings: %.3f  %.3f  %.3f  %.3f \n' % (individual.fitness,
                                                             individual.weights[0],
                                                             individual.weights[1],
                                                             individual.weights[2],
                                                             individual.weights[3]))

        return s






def evolve_weights(pop_size, tournament_size, mutation_rate, mutation_amplitude,
                   elitism, loser_invert, breed_elites, rough_trade, data, max_individuals):
    """Starting with a generation of random portfolios, evolve weights toward the highest rate of return vs risk"""

    # Randomly seed the first generation
    parent_gen = Weights_Population(data ,tournament_size,mutation_rate,mutation_amplitude)
    for i in range(pop_size):
        parent_gen.add_random_individual()

    gen_count = 1
    total_individuals = 0

    # Main evolution loop
    while True :
        parent_gen.sort_population_by_fitness()
        print(parent_gen)
        total_individuals += pop_size
        print(format('%d generations  %d individuals' % (gen_count, total_individuals)))
        print ('\n------------------------------------------\n\n')

        # Success or failure break
        if Weights_Population.halt_evolution(parent_gen, total_individuals, max_individuals):
            break

        # Create empty child generation
        child_gen = Weights_Population(data, tournament_size,mutation_rate, mutation_amplitude)
        start_index = 0

        # Non darwinian methods:
        if elitism:
            # Copy the fittest individual from parent generation to child generation
            elite = copy.deepcopy(parent_gen.individuals[0])
            child_gen.add(elite)
            start_index +=1

        if loser_invert:
            # Take the least fit individual of a generation, invert its DNA and add it to the child generation
            loser = copy.deepcopy(parent_gen.individuals[-1])
            Weights_Population.invert(loser)
            child_gen.add(loser)
            start_index += 1

        if breed_elites:
            # Pairs off the fittest individual with the second fittest, second fittest with third etc
            elite_parents = Weights_Population.elite_mate_selection(parent_gen.individuals, breed_elites)
            for parent_a_index, parent_b_index in elite_parents:
                elite_child = parent_gen.gene_crossover(parent_gen.individuals[parent_a_index], parent_gen.individuals[parent_b_index])
                child_gen.add(elite_child)
                start_index +=1

        if rough_trade:
            # Pairs the fittest and the least fit individuals
            rough_child = parent_gen.gene_crossover(parent_gen.individuals[0], parent_gen.individuals[-1])
            parent_gen.calc_fitness(rough_child)
            child_gen.add(rough_child)
            start_index += 1

        # Having filled in the first n slots of the child generation with special cases, normal breeding
        # now starts. Each remaining slot follows the standard mate-selection and gene crossover procedure
        parent_gen.breed_new_generation(parent_gen, child_gen, start_index)

        # mutate
        child_gen.mutate_pop(start_index)

        parent_gen = child_gen
        gen_count += 1

    return parent_gen.individuals[0], gen_count, total_individuals


def get_data():
    """Returns a Pandas data frame of the daily log-returns for four blue-chip stocks"""

    start = pd.to_datetime('2012-01-01')
    end = pd.to_datetime('2017-01-01')

    # Live Quandl calls
    # aapl = quandl.get('WIKI/AAPL.11', start_date=start, end_date=end)
    # cisco = quandl.get('WIKI/CSCO.11', start_date=start, end_date=end)
    # ibm = quandl.get('WIKI/IBM.11', start_date=start, end_date=end)
    # amzn = quandl.get('WIKI/AMZN.11', start_date=start, end_date=end)

    # Cached historical data
    aapl = pd.read_csv('../data/AAPL_CLOSE', index_col='Date', parse_dates=True)
    cisco = pd.read_csv('../data/CISCO_CLOSE', index_col='Date', parse_dates=True)
    ibm = pd.read_csv('../data/IBM_CLOSE', index_col='Date', parse_dates=True)
    amzn = pd.read_csv('../data/AMZN_CLOSE', index_col='Date', parse_dates=True)

    stocks = pd.concat([aapl, cisco, ibm, amzn], axis=1)
    stocks.columns = ['AAPL', 'CISCO', 'IBM', 'AMZN']

    #print(stocks.head())
    log_ret = np.log(stocks / stocks.shift(1))
    #print(log_ret.head())
    return log_ret


def brute_force():
    """Randomly generates weights. Even with 30,000 individuals the best sharpe ratio is less than the evolved
    solution and considerably more costly in compute time."""
    random_individuals = 10000
    historical_price_data = get_data()
    pop = Weights_Population(historical_price_data, tournament_size=0, mutation_rate=0, mutation_amplitude=0)
    print('generating %d individuals.. ' % random_individuals)
    for i in range(random_individuals):
        pop.add_random_individual()
    print('Sorting by fitness..')
    pop.sort_population_by_fitness()

    print('\nBrute-force optimal:')

    print(pop.individuals[0])

    exp_returns = []
    exp_volatility = []
    sharpes = []
    for individual in pop.individuals:
        exp_returns.append(individual.expected_return)
        exp_volatility.append(individual.expected_volatility)
        sharpes.append(individual.fitness)

    plt.figure(figsize=(12,7))
    plt.scatter(exp_volatility, exp_returns, c=sharpes,cmap='plasma')
    plt.colorbar(label='Sharpe Ratio')
    plt.xlabel('Volatility')
    plt.ylabel('Return')

    optimum_mean_return = exp_returns[0]
    optimum_volatility = exp_volatility[0]

    print(format('Optimum: Sharpe: %.5f   Volatility: %.3f   Mean Return: %.3f')%(sharpes[0], optimum_volatility,optimum_mean_return))
    # Red dot for optimum sharpe ratio
    plt.scatter(optimum_volatility, optimum_mean_return, c='red', s=50, edgecolors='black')
    plt.show()



def evolve_portfolio_weights():
    """Main evolution entry-point"""
    pop_size = 64
    tournament_size = 5
    mutation_rate = 0.7
    """Too little mutation and a population will plateau, too much and the bred survival-advantages 
    are quickly lost in the noise. This setting has been take from various trial runs.
    """

    mutation_amplitude = 1
    max_individuals = 1000
    historical_price_data = get_data()

    # Non-darwinian concepts. See evolve_weights() for details
    elitism = True
    loser_invert = True
    breed_elites = 4
    rough_trade = True


    fittest, gen_count, total_individuals = evolve_weights(pop_size, tournament_size, mutation_rate,
                                                           mutation_amplitude, elitism, loser_invert,
                                                           breed_elites, rough_trade, historical_price_data,
                                                           max_individuals)

    print(fittest)
    print(format('%d individuals in %d generations' % (total_individuals, gen_count)))




if __name__ == '__main__':
    # Compare these two functions to see the advantages of an evolved solution over the brute-force approach.

    # brute_force()
    evolve_portfolio_weights()
