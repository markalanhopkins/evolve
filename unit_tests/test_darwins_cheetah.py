import unittest
from src.darwins_cheetah import evolve_cheetah, Cheetah_Population, Cheetah


class MyTestCase(unittest.TestCase):
    def test_darwins_cheetah(self):
        """Full evolution test"""
        optimal_cheetah_dna = '1110000000000111'
        evolved_cheetah_dna = evolve_cheetah(optimal_cheetah_dna).dna
        self.assertEqual(optimal_cheetah_dna, evolved_cheetah_dna)

    def test_calc_fitness(self):
        optimal = '111000111'
        test_cheetah = Cheetah('110000001')

        cheetah_population = Cheetah_Population(optimal, 0, 0 )
        cheetah_population.calc_fitness(test_cheetah)

        self.assertAlmostEqual(test_cheetah.fitness, 6/9.0)

        test_cheetah = Cheetah('100111000') # Should be 1/9

        cheetah_population.calc_fitness(test_cheetah)
        self.assertEqual(test_cheetah.fitness, 1/9.0)



    def test_mutation(self):
        mutation_rate = 1.0
        cheetah_population = Cheetah_Population('111000111', 0, mutation_rate, )
        test_cheetah = Cheetah('111000111')
        cheetah_population.mutate(test_cheetah)
        self.assertEqual(test_cheetah.dna, '000111000') # With mutation rate of 1 we should have exact invert



    def test_gene_crossover(self):
        p1 = Cheetah('11111111111111111111111111111111')
        p2 = Cheetah('00000000000000000000000000000000')
        cheetah_population = Cheetah_Population('00000000000000000000000000000000', 0, 0)  # Only using to access method

        accumulation = ''
        iterations = 100
        for i in range(iterations):
            child = cheetah_population.gene_crossover(p1,p2)
            accumulation += child.dna

        # We now have a 32 * 100 letter string, check that approximately 0.5 are 1's
        num_zeroes = accumulation.count('0')
        self.assertAlmostEqual(0.5, (num_zeroes / float(len(p1.dna)*iterations)),1)

if __name__ == '__main__':
    unittest.main()
