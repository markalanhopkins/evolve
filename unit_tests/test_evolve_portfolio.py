import unittest
from src.evolve_portfolio import *


class MyTestCase(unittest.TestCase):
    def test_jitter(self):
        i1 = Stock_Weights_Individual(Weights_Population.SYMBOLS, [0.5, 0.1, 0.1, 0.3])
        jitter_amplitude = 1.0
        new_weight_index = 0
        percent_change =  1.0 # jitters weight 0 north by 100% of gap
        new_weight = jitter(minimum=0, current=i1.weights[new_weight_index], maximum=1,
                            amplitude=jitter_amplitude, manual_percent_change=percent_change)
        Weights_Population.rebalance(i1.weights, new_weight, new_weight_index)
        self.assertEqual(i1.weights[0], 1)
        self.assertEqual(1, np.sum(i1.weights))

        i2 = Stock_Weights_Individual(Weights_Population.SYMBOLS, [0.5, 0.1, 0.1, 0.3])
        jitter_amplitude = 1.0
        new_weight_index =  0
        percent_change =  0.5 # jitters weight 0 north by 50% of gap
        new_weight = jitter(minimum=0, current=i2.weights[new_weight_index], maximum=1,
                            amplitude=jitter_amplitude, manual_percent_change=percent_change)
        Weights_Population.rebalance(i2.weights, new_weight, new_weight_index)

        self.assertEqual(i2.weights[0], 0.75)
        self.assertEqual(1, np.sum(i2.weights))

    def test_breed(self):
        a1 = 0.2
        a2 = 0.3
        a3 = 0.1
        a4 = 0.4
        parent_a = Stock_Weights_Individual(Weights_Population.SYMBOLS, [a1, a2, a3, a4])

        b1 = 0.3
        b2 = 0.1
        b3 = 0.4
        b4 = 0.2
        parent_b = Stock_Weights_Individual(Weights_Population.SYMBOLS, [b1, b2, b3, b4])

        expected1 = 0.25
        expected2 = 0.2
        expected3 = 0.25
        expected4 = 0.3

        child = Weights_Population.gene_crossover(parent_a, parent_b)
        for manual, result in zip((expected1, expected2, expected3, expected4), child.weights):
            self.assertAlmostEqual(manual, result, places=10)

        self.assertEqual(expected1 + expected2 + expected3 + expected4, 1.0)

        i3 = Stock_Weights_Individual(Weights_Population.SYMBOLS, [0.5, 0.1, 0.1, 0.3])
        jitter_amplitude = 1.0
        new_weight_index =  0
        percent_change =  -0.5 # jitters weight 0 south by 50% of gap
        new_weight = jitter(minimum=0, current=i3.weights[new_weight_index], maximum=1,
                            amplitude=jitter_amplitude, manual_percent_change=percent_change)
        Weights_Population.rebalance(i3.weights, new_weight, new_weight_index)
        self.assertEqual(i3.weights[0], 0.25)
        self.assertEqual(1, np.sum(i3.weights))


        i4 = Stock_Weights_Individual(Weights_Population.SYMBOLS, [0.5, 0.1, 0.1, 0.3])
        jitter_amplitude = 0.5
        new_weight_index =  0
        percent_change =  -0.5 # jitters weight 0 south by 50% of gap mod amplitude = 25%
        new_weight = jitter(minimum=0, current=i4.weights[new_weight_index], maximum=1,
                            amplitude=jitter_amplitude, manual_percent_change=percent_change)
        Weights_Population.rebalance(i4.weights, new_weight, new_weight_index)
        self.assertEqual(i4.weights[0], 0.5-(0.25 * 0.5 ))
        self.assertEqual(1, np.sum(i4.weights))


    def test_elite_breeding(self):
        i0 = Stock_Weights_Individual(Weights_Population.SYMBOLS, [])
        i0.fitness = 1.1

        i1 = Stock_Weights_Individual(Weights_Population.SYMBOLS, [])
        i1.fitness = 1.1

        i2 = Stock_Weights_Individual(Weights_Population.SYMBOLS, [])
        i2.fitness = 1.05

        i3 = Stock_Weights_Individual(Weights_Population.SYMBOLS, [])
        i3.fitness = 1.05

        i4 = Stock_Weights_Individual(Weights_Population.SYMBOLS, [])
        i4.fitness = 1.02

        i5 = Stock_Weights_Individual(Weights_Population.SYMBOLS, [])
        i5.fitness = 1

        i6 = Stock_Weights_Individual(Weights_Population.SYMBOLS, [])
        i6.fitness = 1

        i7 = Stock_Weights_Individual(Weights_Population.SYMBOLS, [])
        i7.fitness = 1

        i8 = Stock_Weights_Individual(Weights_Population.SYMBOLS, [])
        i8.fitness = 1

        i9 = Stock_Weights_Individual(Weights_Population.SYMBOLS, [])
        i9.fitness = 1

        i10 = Stock_Weights_Individual(Weights_Population.SYMBOLS, [])
        i10.fitness = 0.8

        i11 = Stock_Weights_Individual(Weights_Population.SYMBOLS, [])
        i11.fitness = 0.8

        i12 = Stock_Weights_Individual(Weights_Population.SYMBOLS, [])
        i12.fitness = 0.6

        individuals = [i0,i1,i2,i3,i4,i5,i6,i7,i8,i9,i10,i11,i12]
        card = [(0,2), (2,4),(4,5), (5,10)]
        self.assertEqual(card, Weights_Population.elite_mate_selection(individuals, 4))


    def test_tournament(self):
        """ Partial test for artificially constrained tournament"""
        dummy_pop = Weights_Population(None, 4, 0,  0)

        # create 4 mock Individuals, run a tournament of size 4
        i1 = Stock_Weights_Individual(None, None)
        i1.fitness = 0.1

        i2 = Stock_Weights_Individual(None, None)
        i2.fitness = 0.2

        i3 = Stock_Weights_Individual(None, None)
        i3.fitness = 0.3

        i4 = Stock_Weights_Individual(None, None)
        i4.fitness = 0.4
        # Manually attach individuals
        dummy_pop.individuals = [i1, i2, i3, i4]
        fittest = dummy_pop.tournament()
        self.assertEqual (fittest.fitness,  0.4)


if __name__ == '__main__':
    unittest.main()

